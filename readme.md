## 环境配置(可直接复制粘贴到终端中运行)
### 1. 报告制作独立环境
conda create -n env_name python=3.6
### 2. 激活环境
source activate env_name
### 3. 基于requirements.txt安装依赖包
pip install -r requirements.txt

## 文件说明
1. ImageFactory.py 画图用图像类
2. configs.py 执行main.ipynb前进行参数设置的设置文件
3. main.ipynb 主程序
4. kpdata.py 对keepa数据进行处理和集合
5. jsdata.py 对jungle scout数据进行处理和集合
6. models.py 报告模型
7. template.html 生成报告所用的jinja2模板
8. tools.py 常用工具性函数
9. datas.py 未处理的原始数据读取

报告模板中main.ipynb为分析用的主要ipynb

## 报告制作步骤
1. 修改configs.py中的参数
2. 下载好文件名符合惯例的jungle scout数据
3. 执行main.ipynb
4. 根据初步生成的报告内容修改main.ipynb中的部分自定义内容
5. 对于商品信息中无法正常显示的图片，用Asin作为图片名jpg格式保存入image文件夹
6. 把数据上传到Towel和(服务器)[http://dev-server-analysis:8889/tree/amz-market-trends-analysis]
7. 把报告上传到Towel并@Luoxiangyu 
8. 把审核报告后的任务放到小鹅通上架清单中