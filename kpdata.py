# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import json,math
import logging
import matplotlib.pyplot as plt

from datetime import datetime, date, timedelta
from configs import *
import configs
from datas import jungle_scout_data
from jsdata import jsdata
from tools import SalesEstimator, str2num, convert_to_daily_data, parse_keepa_ts
# logging.basicConfig(filename='example.log',level=logging.INFO)

date_target = date.today().strftime('%Y-%m-%d')
deltaday = timedelta(181)
date_since = (date.today() - deltaday).strftime('%Y-%m-%d')
interval_days = deltaday.days-1
category_name_keepa =  category_name.replace('&','').replace('  ',' ').replace(' ','_').lower()
filename = '{}/data_or_source/product-request-{}.json'.format(folder_path,category_name_keepa)
sales_estimator = SalesEstimator()
with open(filename) as f:
    products = json.loads(f.read())

#Todo: 检查jsdata数据量是否100

trackingSince = {}
added_time = pd.DataFrame()

def market_trends_info():
    
    def fillna(df_asin):
        """根据线性拟合填充评论变化中的缺失数据，假设评论量的增长是线性的"""
        def get_to_not_null_day(df_asin): 
            '''获取评论数从nan到非空的时间'''
            for i in range(len(df_asin)):
                if not np.isnan(df_asin.review_count[i]):
                    return i
            
        def find_break_point(df_asin):
            '''获取评论数突然跃升或者下降的突变时点'''
            for i in range(1,len(df_asin)-1):
                if abs(df_asin.review_count[i]-df_asin.review_count[i-1]) > 100:
                    return i
            return False

        appear_not_null_day = get_to_not_null_day(df_asin)
        if appear_not_null_day > 1 and appear_not_null_day<interval_days-1:
            if find_break_point(df_asin):
                fit_stop_time = find_break_point(df_asin)
                [slope, x0] = np.polyfit(range(appear_not_null_day,fit_stop_time),df_asin.review_count[appear_not_null_day:fit_stop_time],1)
            else:
                [slope, x0] = np.polyfit(range(appear_not_null_day,interval_days),df_asin.review_count[appear_not_null_day:interval_days],1)
            if slope >= -0.01:
                for i in range(interval_days,-1,-1):
                    if np.isnan(df_asin.review_count[i]):
                        review_count_at_i = (i-appear_not_null_day)*slope + df_asin.review_count[appear_not_null_day]
                        if review_count_at_i > 0:
                            df_asin.loc[i,'review_count'] = review_count_at_i
                        elif df_asin.review_count[i+1] > 0:
                            df_asin.loc[i,'review_count'] = 0.0

        asin = df_asin.asin[0]
        for i in range(len(df_asin)):
            if not np.isnan(df_asin.review_count[i]):
                
                first_review_time = datetime.strptime(df_asin.date[i], '%Y-%m-%d')
                if added_time.loc[asin, 'trackingSince'] > first_review_time:
                    added_time.loc[asin,'added_time'] = first_review_time
                    added_time.loc[asin, 'trackingSince'] = first_review_time
                    added_time.loc[asin,'review_count'] = df_asin.review_count[i]
                    logging.debug(u'{}评论出现时间早于追踪时间，更替上架时间为评论时间{}'.format(asin, first_review_time))
                break

    def fill_review_na(df_market_trends):
        for asin in set(df_market_trends.asin):
            if df_market_trends[df_market_trends.asin == asin].review_count.max()>0: 
                df_asin = df_market_trends[df_market_trends.asin == asin].reset_index()
                fillna(df_asin)

                df_market_trends.loc[df_market_trends.asin == asin,'review_count'] = df_asin['review_count'].values
        return df_market_trends
    def count_nan(df_market_trends):
        nan_count = {'review_count':np.isnan(df_market_trends.review_count).sum()/interval_days,
                     'price':np.isnan(df_market_trends.price).sum()/interval_days,
                     'bsr':np.isnan(df_market_trends.bsr).sum()/interval_days}
        content = 'Missing Data:{}\n'.format('='*16)
        df_market_trends.nan = nan_count
        for key in nan_count.keys():
            content += '{}{}: {}\n'.format('\t'*2,key.capitalize(),nan_count[key])
        logging.warning(content)

    def get_productGroup(product):
        if product['productGroup'] in product_groups:
            return product['productGroup']
        for i in range(product_groups.__len__()):
            product = products[i]
            if product['productGroup'] in product_groups:
                return product['productGroup']
        raise BaseException("Can't find the productgroup in existed") 
    asins = []
    columns = ['asin', 'bsr', 'date', 'estimate_sales', 'price', 'rating', 'review_count']
    df_market_trends = pd.DataFrame(columns = columns)
    df_market_trends_tmp = pd.DataFrame(columns = columns)
    #product_group = products[0]['productGroup']

    global added_time
    for i in range(products.__len__()):
        product = products[i]
        asin = product['asin']
        added_time.loc[asin, 'trackingSince'] = parse_keepa_ts(product['trackingSince'])
        if not asin in asins:
            asins.append(asin)
            df_market_trends_tmp['date'] = convert_to_daily_data(product['csv'][3],date_since,date_target).keys()
            df_market_trends_tmp['bsr'] = convert_to_daily_data(product['csv'][3],date_since,date_target).values()
            df_market_trends_tmp['price'] = convert_to_daily_data(product['csv'][1],date_since,date_target).values()
            for date in range(182):
                price = df_market_trends_tmp.loc[date,'price']
                df_market_trends_tmp.loc[date,'price'] = price if price and price<100000 else float('nan')
            df_market_trends_tmp['review_count'] = convert_to_daily_data(product['csv'][17],date_since,date_target).values()
            df_market_trends_tmp['rating'] = convert_to_daily_data(product['csv'][16],date_since,date_target).values()
            df_market_trends_tmp['asin'] = asin

            df_market_trends_tmp['estimate_sales'] = [sales_estimator.estimate(asin, rank) for rank in df_market_trends_tmp['bsr']]
            df_market_trends = df_market_trends.append(df_market_trends_tmp.sort_values(by='date'))
    df_market_trends.fillna(value=np.nan, inplace=True)
    df_market_trends['estimate_sales'] = df_market_trends['estimate_sales']/30.0
    df_market_trends['price'] = df_market_trends['price']/100.0
    df_market_trends['rating'] = df_market_trends['rating']/10.0
    count_nan(df_market_trends)
    return fill_review_na(df_market_trends)

df_market_trends = market_trends_info()

def get_correct_coefficient():
    last_day = date_target
    indicator_trend_columns = ['date','price','review_count_mean','review_count_sum','rating','bsr','estimate_sales']
    correct_coefficient = {column:1 for column in indicator_trend_columns}
    indicator_trend_last_day = {}
    indicator_trend_last_day['price'] = df_market_trends.price[df_market_trends.date == last_day].mean()
    indicator_trend_last_day['rating'] = df_market_trends.rating[df_market_trends.date == last_day].mean()
    indicator_trend_last_day['review_count_mean'] = df_market_trends.review_count[df_market_trends.date == last_day].mean()
    indicator_trend_last_day['review_count_sum'] = indicator_trend_last_day['review_count_mean']*100
    indicator_trend_last_day['bsr'] = df_market_trends.bsr[df_market_trends.date == last_day].mean()
    indicator_trend_last_day['estimate_sales'] = df_market_trends.estimate_sales[df_market_trends.date == last_day].mean()*100
    jungle_scout = {}
    jungle_scout['price'] = jsdata.price.mean
    jungle_scout['rating'] = jsdata.rating.mean
    jungle_scout['review_count_mean'] = jsdata.review.mean
    jungle_scout['review_count_sum'] = jsdata.review.mean*100
    jungle_scout['bsr'] = np.nanmean(jsdata.data['Rank'])
    jungle_scout['estimate_sales'] = np.nansum(jsdata.data['Sales'])/30
    for indicator in indicator_trend_last_day.keys():
        correct_coefficient[indicator] = jungle_scout[indicator]/indicator_trend_last_day[indicator]
        if correct_coefficient[indicator] < 0.9 or correct_coefficient[indicator] > 1.1:
            logging.warning('Correct Coefficient Excepting: %s -> %f'%(indicator, correct_coefficient[indicator]))
    return correct_coefficient

correct_coefficient = get_correct_coefficient()

def get_indicator_trend():
    cursor_date = datetime.strptime(date_since, '%Y-%m-%d')
    indicator_trend_columns = ['date','price','review_count_mean','review_count_sum','rating','bsr','estimate_sales']
    indicator_trend = pd.DataFrame(columns = indicator_trend_columns)
    review_count_sum_tmp = 0
    cursor_date = datetime.strptime(date_since, '%Y-%m-%d')
    dict_indicator_trend = {}
    dict_indicator_trend['review_count_sum'] = 0
    while cursor_date<datetime.strptime(date_target, '%Y-%m-%d'):
        cursor_date_str = cursor_date.strftime('%Y-%m-%d')
        dict_indicator_trend['date'] = cursor_date_str
        dict_indicator_trend['price'] = df_market_trends.price[df_market_trends.date == cursor_date_str].mean()
        dict_indicator_trend['rating'] = df_market_trends.rating[df_market_trends.date == cursor_date_str].mean()
        dict_indicator_trend['bsr'] = df_market_trends.bsr[df_market_trends.date == cursor_date_str].mean()
        dict_indicator_trend['estimate_sales'] = df_market_trends.estimate_sales[df_market_trends.date == cursor_date_str].mean()*100
        
        review_count_cursor_date = df_market_trends.review_count[df_market_trends.date == cursor_date_str]
        review_count_sum_cursor_date = review_count_cursor_date.sum()
        #print('{}:{}'.format(cursor_date,review_count_sum_cursor_date))
        review_count_sum_change = review_count_sum_cursor_date - review_count_sum_tmp
        if review_count_sum_change >-0.05*review_count_sum_tmp and (review_count_sum_change < 0.1*review_count_sum_tmp or cursor_date==datetime.strptime(date_since, '%Y-%m-%d')):
            dict_indicator_trend['review_count_sum'] += review_count_sum_change
        else:
            logging.info('{}:{}'.format(cursor_date,review_count_sum_change))
        review_count_sum_tmp = review_count_sum_cursor_date #update tmp
        review_number_count = review_count_cursor_date.__len__()-review_count_cursor_date.isnull().sum()
        dict_indicator_trend['review_count_mean'] = dict_indicator_trend['review_count_sum']/review_number_count
        corrected_indicator_trend = {indicator:dict_indicator_trend[indicator]*correct_coefficient[indicator] for indicator in correct_coefficient.keys()}
        indicator_trend = indicator_trend.append(pd.Series(corrected_indicator_trend),ignore_index=True)
        cursor_date += timedelta(days=1)

    indicator_trend.insert(3,'price_rate',np.insert(indicator_trend.price[1:].values - indicator_trend.price[0:interval_days].values,0,None))
    indicator_trend.insert(3,'review_count_sum_rate',np.insert(indicator_trend.review_count_sum[1:].values - indicator_trend.review_count_sum[0:interval_days].values,0,None))
    indicator_trend.insert(3,'rating_rate',np.insert(indicator_trend.rating[1:].values - indicator_trend.rating[0:interval_days].values,0,None))
    indicator_trend.insert(3,'bsr_rate',np.insert(indicator_trend.bsr[1:].values - indicator_trend.bsr[0:interval_days].values,0,None))
    df_indicator_trend = indicator_trend[['date','price','price_rate','review_count_mean','review_count_sum','review_count_sum_rate','rating','rating_rate','bsr','bsr_rate','estimate_sales']]
    df_indicator_trend.columns = ['日期','平均价格($)','平均价格变动量($)','平均评论数','评论总数','评论总数变动量','平均评论星级','平均评论星级变动量','平均BSR','平均BSR变动量','预测销售量']
    df_indicator_trend.to_csv('{}/data_or_source/indicator_trend_'.format(folder_path)+str(interval_days)+'.csv', encoding='utf-8')
    return df_indicator_trend

df_indicator_trend = get_indicator_trend()

df_indicator_statistic = pd.read_csv('../trend_indicator_statistic/trend_indicator_statistic.csv',index_col=0)

class KPData(object):
    """The data from Keepa"""
    def __init__(self, df_indicator_trend):
        self.indicator_trend = df_indicator_trend
        self.price = self.Price(self.indicator_trend['平均价格($)'])
        self.review = self.Review(self.indicator_trend['评论总数'])
        self.rating = self.Rating(self.indicator_trend['平均评论星级'])
        self.added_time = self.Added_time(added_time)


    class Price(object):
        result = {'price_std': [u'很小', u'较小', u'较大', u'很大']}
        def __init__(self, data):
            self.data = data
            self.mean = data.mean()
            self.max = data.max()
            self.min = data.min()
            self.std = self.get_column_std(data)
            self.std_verdict = self.value_verdict(self.std, value_name = 'price_std')
            self.review = ''
            self.review_added = ''
        @classmethod
        def get_column_std(self, array):
            return ((array - array.min())/(array.max()-array.min())).std()
        def value_verdict(self, value, proportion=16, value_name = None):
            global df_indicator_statistic
            def get_span(series):
                return [np.percentile(series,i) for i in [proportion,50,100-proportion]]
            def discrimination_result(constant,discrimination):
                for i in range(3):
                    if constant < discrimination[i]:
                        return self.result[value_name][i]
                return self.result[value_name][3]
            
            df_indicator_statistic.loc[category_name, value_name] = value
            span_discrimination = get_span(df_indicator_statistic.loc[:, value_name])
            return discrimination_result(value, span_discrimination)
        
    class Review(Price):
        result = {'review_grow_rate': [u'很慢', u'较慢', u'较快', u'很快']}
        def __init__(self, data):
            self.data = data
            self.grow_rate = self.get_review_grow_rate()
            self.grow_rate_verdict = self.value_verdict(self.grow_rate, value_name = 'review_grow_rate')
        
        def get_review_grow_rate(self):
            review_first_day = self.data[0]
            review_last_day = self.data[180]
            return review_last_day/review_first_day - 1

        def plot_top5(self):
            plt.figure()
            for asin in jsdata.data['ASIN'][0:5]:
                try:
                    df_market_trends[df_market_trends.asin == asin].plot(x='date',y='review_count',sharex=True)
                    plt.legend([asin])
                except TypeError as e:
                    print(asin + ':\n')
                    print(e)
            plt.show()

    class Rating(Price):
        result = {'rating_std': [u'很小', u'较小', u'较大', u'很大'],
                 'market_preference_stability': [u'非常稳定', u'较为稳定', u'较为不稳', u'很不稳定']}
        def __init__(self, data):
            self.data = data
            self.std = self.data.std()
            self.std_verdict = self.value_verdict(self.std, value_name = 'rating_std')
            self.market_preference_stability = self.result['market_preference_stability'][self.result['rating_std'].index(self.std_verdict)]

    class Added_time(object):
        result = {'update_rate': list(reversed([u'很慢', u'较慢', u'较快', u'很快']))}
        def __init__(self, data):
            self.data = data
            self.update_rate = self.discrimination_result(self.added_duration_mean, 'update_rate') # 商品更新速度
            try:
                self.added_product = self.data[self.data.trackingSince>(datetime.now() - deltaday)]
            except BaseException as e:
                logging.warning(e)


        @property
        def added_product_count(self):
            count = self.added_product.__len__()
            if count > 30:
                logging.warning("新上架商品数大于30: {}".format(count))
            return count    


        @property 
        def top5_added_text(self):
            new_product_count = 0
            text = [u"其中，"]
            for i in range(5):
                chinese_number = {1:u'一', 2:u'二',3:u'三',4:u'四',5:u'五'}
                asin = jsdata.data.ASIN[i]
                if self.data.loc[asin, 'trackingSince'] > (datetime.today()-timedelta(interval_days)):
                    new_product_count += 1
                    if new_product_count < 2:
                        text.append(u"榜单排名第{}的商品{}".format(chinese_number[i+1],asin))
                    else:
                        text.append(u"和榜单排名第{}的商品{}".format(chinese_number[i+1],asin))
            text.append(u"上架时间很短，上架时间不足半年。")
            if new_product_count > 0:
                return ''.join(text)
            else:
                return ''

        @property
        def added_duration_mean(self):
            """平均上架时长"""
            return np.mean(self.data.trackingSince.map(lambda x:datetime.today() - x)).days

        def discrimination_result(self, constant, value_name):
            discrimination = [300, 450, 600]
            for i in range(3):
                if constant < discrimination[i]:
                    return self.result[value_name][i]
            return self.result[value_name][3]

    def __call__(self):
        print(u'预测月销售量为%d'%(df_indicator_trend['预测销售量'][interval_days]*30))
        df_indicator_statistic.to_csv('../trend_indicator_statistic/trend_indicator_statistic.csv')
        
        logging.info('The trend indicator statistic data has been saved') 

kpdata = KPData(df_indicator_trend)